// Requires
const express     = require('express');
const app         = express();
const port        = 3000;
const pug         = require('pug');
const path        = require('path');
const sass        = require('node-sass-middleware');
const Logger      = require('./helpers/Logger');
const vnb         = require('./helpers/viewNameBuilder');

// Middlewares
app.use(sass({
  src: path.join(__dirname, 'src'),
  dest: path.join(__dirname, 'public'),
  debug: true,
  outputStyle: 'compressed',
  prefix: '/assets',
  log: (severity, key, val) => Logger.debug(`node-saas-middleware ${key} : ${val}`)
}));
app.use('/assets', express.static(path.join(__dirname, 'public'),{redirect: false} ));

// Actions
// Dummy action
app.get('/', (req, res) => {
  const name = "there"

  // Response to the client
  res.status(200).send(pug.renderFile(vnb('index'), {name}));
})

// Initialization
app.listen(port, () => { console.log(`Started at ${port}`) })

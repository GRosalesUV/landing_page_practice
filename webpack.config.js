const path = require('path');

module.exports = {
  entry: './src/js/application.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public', 'js'),
  },
};
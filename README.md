## Landing Page practice

This landing page shows a freelance portfolio, contact information and links to his/her social media for more content. 

Current work is made for practice Frontend technologies.

### Current stack for development
- [Nodejs](https://nodejs.org/en/download/)
- [Pug.js Templating)](https://pugjs.org/api/getting-started.html)
- [Sass (CSS with superpowers)](https://www.npmjs.com/package/node-sass-middleware)
- [Expressjs (Backend)](https://expressjs.com/es/)
- [Bootstrap (Frondend library)](https://getbootstrap.com/)

### Extra information goes below

This page is made with love ❤️ (and some effort 😉).


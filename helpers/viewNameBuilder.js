const viewNameBuilder = (viewPathName) => `${__dirname}/../views/${viewPathName}.pug`;

module.exports = viewNameBuilder;
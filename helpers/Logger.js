const log4js = require("log4js");
log4js.configure({
    appenders: {
        appDev: { type: "file", filename: "app-dev.log" },
        appProd: { type: "file", filename: "app-prod.log" },
    },
    categories: { default: { appenders: [`app${(process.env.NODE_ENV || 'development') === 'production' ? "Prod" : "Dev"}`], level: "debug" } }
});
const Logger = log4js.getLogger("app");

module.exports = Logger;
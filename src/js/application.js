import 'bootstrap';

// Show portfolio modal function
const showPortfolioImage = (e) => {
  const imageSource = e.relatedTarget.nextSibling.src;
  const imageContainer = e.target.querySelector('.modal-body .img-container');

  imageContainer.src = imageSource;

  return;
};

// Upside Arrow functions
const handleUpsideArrow = () => {
  const upsideArrow = document.querySelector(".upside-arrow");

  (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) ? showUpsideArrow(upsideArrow) : hideUpsideArrow(upsideArrow);
  return ;
};
const showUpsideArrow = el => {
  if(!el.classList.contains("hidden"))
    return ;

  el.classList.add("hide");
  el.classList.remove("hidden");
  el.classList.remove("hide");
};
const hideUpsideArrow = el => {
  if(el.classList.contains("hidden"))
    return ;

  el.classList.add("hide");
  setTimeout(() => {
    el.classList.add("hidden");
    el.classList.remove("hide");
  }, 190)
};

// DOMContentLoaded event
document.addEventListener('DOMContentLoaded', () => {
  const portfolioModal = document.querySelector('#portfolioModal');

  portfolioModal.addEventListener('show.bs.modal', showPortfolioImage);
  window.onscroll = handleUpsideArrow;
});